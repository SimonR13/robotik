# Data: {"nodes":[{"id":12,"location":{"y":80,"x":100},"type":"start"},{"id":17,"module":"Pflichtuebung2/aufgabe4.py","instanceName":"initclass","location":{"y":100,"x":260},"priority":50,"actionName":"initClass","parameters":{},"type":"action"},{"id":18,"module":"Pflichtuebung2/aufgabe4.py","instanceName":"actiongo","location":{"y":140,"x":560},"priority":50,"actionName":"ActionGo","parameters":{},"type":"action"}],"connections":[{"slotNode":17,"signal":{"normalParams":0,"name":"start","namedParams":[]},"bendpoints":[],"slot":{"parametersOptional":[],"name":"start","parametersRequired":[]},"signalNode":12},{"slotNode":18,"signal":{"normalParams":0,"name":"stopped","namedParams":[]},"bendpoints":[],"slot":{"parametersOptional":[],"name":"start","parametersRequired":[]},"signalNode":17}]}
from aria_python.robot import getRobotInstance, ExecuteActionsFlag
from pydispatch import dispatcher
import Pflichtuebung2.aufgabe4

if ExecuteActionsFlag:
    robot = getRobotInstance()
    action_initclass = Pflichtuebung2.aufgabe4.initClass()
    action_initclass.__action_name__ = 'initclass'
    action_initclass.__diagram_name__ = 'Pflichtuebung2/aufgabe4_diagram.py'
    robot.addAction(action_initclass, 50)

    action_actiongo = Pflichtuebung2.aufgabe4.ActionGo()
    action_actiongo.__action_name__ = 'actiongo'
    action_actiongo.__diagram_name__ = 'Pflichtuebung2/aufgabe4_diagram.py'
    robot.addAction(action_actiongo, 50)

    dispatcher.connect(action_actiongo.start, sender=action_initclass, signal='stopped')
    action_initclass.start()
