'''
Created on 20.11.2013

@author: rftpool22
'''

from AriaPy import ArActionDesired
from aria_python.robot import getRobotInstance
from aria_python.adaction import AdAction
from Pflichtuebung2.initGoals import getGoals
from de.trier.function.utility import get_lines
from de.trier.function.utility import build_graph
from BaseArnlPy import ArPathPlanningTask

global graph

class initClass(AdAction):

    def __init__(self):
        AdAction.__init__(self)
        self._desired = ArActionDesired()
        self.initPath()
        
    def initPath(self):
            # Get PathTask
        path_task=getRobotInstance().pathTask
        # Get Map
        ar_map=path_task.getAriaMap()
        
        self.goals = getGoals()
        self.lines = get_lines(ar_map)
        
        self._graph = build_graph(self.goals, self.lines)
        global graph
        graph = self._graph
        
    def fire(self, currentDesired):
        global graph
        
        print(graph)
        
        self.stop()

class ActionGo(AdAction):
    
    def __init__(self):
        AdAction.__init__(self)
        self._desired = ArActionDesired()
        self.goal = 21
        self._goal = 0
        
        
    def dijkstra(self):
        m = len(self.goals)
        d = [None]*m                # (geschaetzte) Pfadkosten
        d[self.goal] = 0                    # sind 0 fuer den Startknoten
        Q = {u for u in range(m)}   # alle Knoten
        while Q:
            # Auswahl von v mit kleinstem d-Wert
            (_,v) = min({(d[u],u) for u in Q if d[u]!= None})
            # v aus Q entfernen, d[v] ist endgueltig
            Q.remove(v)
            # Schaetzungen fuer Nachfolger von v aktualisieren
            for u in self.goals[v]:
                alt = d[v] + self.goals[v][u]            # alternative Kosten
                if d[u]==None or alt < d[u]:
                    d[u] = alt
        self._d
    
    def start(self):
        AdAction.start(self)
        pathTask = getRobotInstance().pathTask
        
        pathTask.pathPlanToPose(self._d[self._goal], False)
        self._goal += 1
        
    def fire(self, currentDesired):
        pathTask = getRobotInstance().pathTask
        
