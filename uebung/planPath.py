'''
Created on 20.11.2013

@author: rftpool22
'''
from aria_python.robot import getRobotInstance
from Pflichtuebung2.initGoals import getGoals
from de.trier.function.utility import get_lines
from de.trier.function.utility import build_graph

def initPath():
        # Get PathTask
    path_task=getRobotInstance().pathTask
    # Get Map
    ar_map=path_task.getAriaMap()
    
    goals = getGoals()
    lines = get_lines(ar_map)
    
    graph = build_graph(goals, lines)
    
    print(graph)
    
    return graph