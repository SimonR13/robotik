from aria_python.adaction import AdAction
from AriaPy import ArActionDesired
from aria_python.robot import getRobotInstance

class ActionGo(AdAction):
    
    def __init__(self):
        AdAction.__init__(self)
        self._desired = ArActionDesired()
        self.goal = 0
        
        
    def dijkstra(self):
        m = len(self.goals)
        d = [None]*m                # (geschaetzte) Pfadkosten
        d[self.goal] = 0                    # sind 0 fuer den Startknoten
        Q = {u for u in range(m)}   # alle Knoten
        while Q:
            # Auswahl von v mit kleinstem d-Wert
            (_,v) = min({(d[u],u) for u in Q if d[u]!= None})
            # v aus Q entfernen, d[v] ist endgueltig
            Q.remove(v)
            # Schaetzungen fuer Nachfolger von v aktualisieren
            for u in self.goals[v]:
                alt = d[v] + self.goals[v][u]            # alternative Kosten
                if d[u]==None or alt < d[u]:
                    d[u] = alt
        return d

    d=dijkstra()
    
    def start(self):
        AdAction.start(self)
        pathTask = getRobotInstance().pathTask
        
        pathTask.pathPlanToPose([self._goal], False)
        self.goal+=1
        
    def fire(self, currentDesired):
        pathTask = getRobotInstance().pathTask