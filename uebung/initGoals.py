'''
Created on 20.11.2013

@author: rftpool22
'''
from aria_python.robot import getRobotInstance
from AriaPy import ArMapObject

def getGoals():
# Get PathTask
    path_task=getRobotInstance().pathTask
# Get Map
    ar_map=path_task.getAriaMap()
# Get Tuple of Goals
    goals = sorted(ar_map.findMapObjectsOfType("Goal") + \
                    ar_map.findMapObjectsOfType("GoalWithHeading"), key = ArMapObject.getName, reverse = True)
    
    return goals