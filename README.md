# Robotics / Robotik
Finals project of robotics class

- moving to first object
- picking it up
- moving to drop off zone without hitting some obstacles
- dropping the object off
- repeating until pick-up-zone empty
- while moving back to pick-up-zone
    - checking if already placed object changed
    - if object has changed, picking new object up
    - bring new object to pick-up-zone
- avoid stationary and moving obstacles
