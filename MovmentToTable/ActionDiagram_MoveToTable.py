# Data: {"nodes":[{"id":59,"location":{"y":120,"x":60},"type":"start"},{"id":60,"module":"MovmentToTable/Path.py","instanceName":"movetotable","location":{"y":60,"x":320},"priority":50,"actionName":"MoveToTable","parameters":{"deliverGoal":"1"},"type":"action"},{"id":61,"module":"MovmentToTable/Path.py","instanceName":"initmovetotable","location":{"y":60,"x":180},"priority":50,"actionName":"initMoveToTable","parameters":{},"type":"action"},{"id":62,"module":"de/trier/directaction/turn.py","instanceName":"turn","location":{"y":150,"x":800},"priority":50,"actionName":"Turn","parameters":{"angle":"-90"},"type":"action"},{"id":63,"module":"de/trier/directaction/move.py","instanceName":"move","location":{"y":140,"x":680},"priority":50,"actionName":"Move","parameters":{"distance":"900"},"type":"action"},{"id":64,"module":"gripper/GripperDrop.py","instanceName":"actiondrop","location":{"y":375,"x":375},"priority":50,"actionName":"ActionDrop","parameters":{},"type":"action"},{"id":65,"module":"gripper/GripperControl.py","instanceName":"grippercontrol","location":{"y":375,"x":50},"priority":50,"actionName":"GripperControl","parameters":{},"type":"action"},{"id":66,"module":"gripper/GripperMove.py","instanceName":"actionmove","location":{"y":400,"x":675},"priority":50,"actionName":"ActionMove","parameters":{},"type":"action"},{"id":67,"module":"de/trier/directaction/turn.py","instanceName":"turn1","location":{"y":-20,"x":640},"priority":50,"actionName":"Turn","parameters":{"angle":"-90"},"type":"action"}],"connections":[{"slotNode":60,"signal":{"normalParams":0,"name":"stopped","namedParams":[]},"bendpoints":[{"y":120,"x":300},{"y":120,"x":320}],"slot":{"parametersOptional":[],"name":"start","parametersRequired":[]},"signalNode":61},{"slotNode":60,"signal":{"normalParams":1,"name":"goals","namedParams":[]},"bendpoints":[],"slot":{"parametersOptional":[],"name":"on_set_goals","parametersRequired":["goals"]},"signalNode":61},{"slotNode":61,"signal":{"normalParams":0,"name":"start","namedParams":[]},"bendpoints":[],"slot":{"parametersOptional":[],"name":"start","parametersRequired":[]},"signalNode":59},{"slotNode":62,"signal":{"normalParams":0,"name":"stopped","namedParams":[]},"bendpoints":[],"slot":{"parametersOptional":[],"name":"start","parametersRequired":[]},"signalNode":63},{"slotNode":63,"signal":{"normalParams":0,"name":"stopped","namedParams":[]},"bendpoints":[{"y":60,"x":760},{"y":100,"x":760},{"y":100,"x":660},{"y":200,"x":660}],"slot":{"parametersOptional":[],"name":"start","parametersRequired":[]},"signalNode":67},{"slotNode":63,"signal":{"normalParams":0,"name":"noTurn","namedParams":[]},"bendpoints":[{"y":220,"x":660},{"y":200,"x":660}],"slot":{"parametersOptional":[],"name":"start","parametersRequired":[]},"signalNode":60},{"slotNode":64,"signal":{"normalParams":0,"name":"stopped","namedParams":[]},"bendpoints":[{"y":250,"x":975},{"y":325,"x":975},{"y":325,"x":325},{"y":450,"x":325}],"slot":{"parametersOptional":[],"name":"start","parametersRequired":[]},"signalNode":62},{"slotNode":64,"signal":{"normalParams":1,"name":"GripState","namedParams":[]},"bendpoints":[],"slot":{"parametersOptional":[],"name":"on_gripState","parametersRequired":["gs"]},"signalNode":65},{"slotNode":64,"signal":{"normalParams":0,"name":"gripper_stopped","namedParams":[]},"bendpoints":[{"y":575,"x":325},{"y":550,"x":325}],"slot":{"parametersOptional":[],"name":"on_gripperClosed","parametersRequired":[]},"signalNode":65},{"slotNode":64,"signal":{"normalParams":1,"name":"paddleState","namedParams":[]},"bendpoints":[{"y":600,"x":350},{"y":575,"x":350}],"slot":{"parametersOptional":[],"name":"on_paddleState","parametersRequired":["ps"]},"signalNode":65},{"slotNode":64,"signal":{"normalParams":0,"name":"stopped","namedParams":[]},"bendpoints":[{"y":500,"x":875},{"y":350,"x":875},{"y":350,"x":350},{"y":500,"x":350}],"slot":{"parametersOptional":[],"name":"on_afterMove","parametersRequired":[]},"signalNode":66},{"slotNode":65,"signal":{"normalParams":0,"name":"stopped","namedParams":[]},"bendpoints":[{"y":250,"x":950},{"y":300,"x":950},{"y":300,"x":25},{"y":450,"x":25}],"slot":{"parametersOptional":[],"name":"start","parametersRequired":[]},"signalNode":62},{"slotNode":65,"signal":{"normalParams":0,"name":"LowerGripper","namedParams":[]},"bendpoints":[{"y":550,"x":650},{"y":675,"x":650},{"y":675,"x":75},{"y":675,"x":0},{"y":575,"x":0},{"y":575,"x":25}],"slot":{"parametersOptional":[],"name":"on_lowerGripper","parametersRequired":[]},"signalNode":64},{"slotNode":65,"signal":{"normalParams":0,"name":"OpenGripper","namedParams":[]},"bendpoints":[{"y":575,"x":725},{"y":650,"x":725},{"y":650,"x":100},{"y":650,"x":25},{"y":600,"x":25}],"slot":{"parametersOptional":[],"name":"on_openGripper","parametersRequired":[]},"signalNode":64},{"slotNode":66,"signal":{"normalParams":1,"name":"GoBack","namedParams":[]},"bendpoints":[{"y":500,"x":625},{"y":525,"x":625}],"slot":{"parametersOptional":[],"name":"on_goDistance","parametersRequired":["dist"]},"signalNode":64},{"slotNode":66,"signal":{"normalParams":0,"name":"stopped","namedParams":[]},"bendpoints":[{"y":475,"x":650},{"y":500,"x":650}],"slot":{"parametersOptional":[],"name":"stop","parametersRequired":[]},"signalNode":64},{"slotNode":66,"signal":{"normalParams":1,"name":"GoForward","namedParams":[]},"bendpoints":[],"slot":{"parametersOptional":[],"name":"on_goDistance","parametersRequired":["dist"]},"signalNode":64},{"slotNode":67,"signal":{"normalParams":0,"name":"TURN","namedParams":[]},"bendpoints":[{"y":100,"x":560},{"y":40,"x":600}],"slot":{"parametersOptional":[],"name":"start","parametersRequired":[]},"signalNode":60}]}
from aria_python.robot import getRobotInstance, ExecuteActionsFlag
from pydispatch import dispatcher
import gripper.GripperDrop
import de.trier.directaction.move
import gripper.GripperControl
import gripper.GripperMove
import de.trier.directaction.turn
import MovmentToTable.Path

if ExecuteActionsFlag:
    robot = getRobotInstance()
    action_movetotable = MovmentToTable.Path.MoveToTable(deliverGoal = 1)
    action_movetotable.__action_name__ = 'movetotable'
    action_movetotable.__diagram_name__ = 'MovmentToTable/ActionDiagram_MoveToTable.py'
    robot.addAction(action_movetotable, 50)

    action_initmovetotable = MovmentToTable.Path.initMoveToTable()
    action_initmovetotable.__action_name__ = 'initmovetotable'
    action_initmovetotable.__diagram_name__ = 'MovmentToTable/ActionDiagram_MoveToTable.py'
    robot.addAction(action_initmovetotable, 50)

    action_turn = de.trier.directaction.turn.Turn(angle = -90)
    action_turn.__action_name__ = 'turn'
    action_turn.__diagram_name__ = 'MovmentToTable/ActionDiagram_MoveToTable.py'
    robot.addAction(action_turn, 50)

    action_move = de.trier.directaction.move.Move(distance = 900)
    action_move.__action_name__ = 'move'
    action_move.__diagram_name__ = 'MovmentToTable/ActionDiagram_MoveToTable.py'
    robot.addAction(action_move, 50)

    action_actiondrop = gripper.GripperDrop.ActionDrop()
    action_actiondrop.__action_name__ = 'actiondrop'
    action_actiondrop.__diagram_name__ = 'MovmentToTable/ActionDiagram_MoveToTable.py'
    robot.addAction(action_actiondrop, 50)

    action_grippercontrol = gripper.GripperControl.GripperControl()
    action_grippercontrol.__action_name__ = 'grippercontrol'
    action_grippercontrol.__diagram_name__ = 'MovmentToTable/ActionDiagram_MoveToTable.py'
    robot.addAction(action_grippercontrol, 50)

    action_actionmove = gripper.GripperMove.ActionMove()
    action_actionmove.__action_name__ = 'actionmove'
    action_actionmove.__diagram_name__ = 'MovmentToTable/ActionDiagram_MoveToTable.py'
    robot.addAction(action_actionmove, 50)

    action_turn1 = de.trier.directaction.turn.Turn(angle = -90)
    action_turn1.__action_name__ = 'turn1'
    action_turn1.__diagram_name__ = 'MovmentToTable/ActionDiagram_MoveToTable.py'
    robot.addAction(action_turn1, 50)

    dispatcher.connect(action_movetotable.start, sender=action_initmovetotable, signal='stopped')
    dispatcher.connect(action_movetotable.on_set_goals, sender=action_initmovetotable, signal='goals')
    dispatcher.connect(action_turn.start, sender=action_move, signal='stopped')
    dispatcher.connect(action_move.start, sender=action_turn1, signal='stopped')
    dispatcher.connect(action_move.start, sender=action_movetotable, signal='noTurn')
    dispatcher.connect(action_actiondrop.start, sender=action_turn, signal='stopped')
    dispatcher.connect(action_actiondrop.on_gripState, sender=action_grippercontrol, signal='GripState')
    dispatcher.connect(action_actiondrop.on_gripperClosed, sender=action_grippercontrol, signal='gripper_stopped')
    dispatcher.connect(action_actiondrop.on_paddleState, sender=action_grippercontrol, signal='paddleState')
    dispatcher.connect(action_actiondrop.on_afterMove, sender=action_actionmove, signal='stopped')
    dispatcher.connect(action_grippercontrol.start, sender=action_turn, signal='stopped')
    dispatcher.connect(action_grippercontrol.on_lowerGripper, sender=action_actiondrop, signal='LowerGripper')
    dispatcher.connect(action_grippercontrol.on_openGripper, sender=action_actiondrop, signal='OpenGripper')
    dispatcher.connect(action_actionmove.on_goDistance, sender=action_actiondrop, signal='GoBack')
    dispatcher.connect(action_actionmove.stop, sender=action_actiondrop, signal='stopped')
    dispatcher.connect(action_actionmove.on_goDistance, sender=action_actiondrop, signal='GoForward')
    dispatcher.connect(action_turn1.start, sender=action_movetotable, signal='TURN')
    action_initmovetotable.start()
