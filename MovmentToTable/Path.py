from AriaPy import ArActionDesired
from AriaPy import ArMapObject
from aria_python.robot import getRobotInstance
from aria_python.adaction import AdAction
from de.trier.function.utility import extract_pose
from BaseArnlPy import ArPathPlanningTask
from de.trier.function.directaction import turn
import time


class initMoveToTable(AdAction):

    def __init__(self):
        AdAction.__init__(self)
        self._desired = ArActionDesired()
        self._goals=self.getGoals()
        
    def getGoals(self):
        # Get PathTask
        path_task=getRobotInstance().pathTask
        # Get Map
        ar_map=path_task.getAriaMap()
        # Get Tuple of Goals
        self.goals = sorted(ar_map.findMapObjectsOfType("Goal") + \
                        ar_map.findMapObjectsOfType("GoalWithHeading"), key = ArMapObject.getName, reverse = False)
        return self.goals
              
    def fire(self, currentDesired):
        self.signal("goals", self._goals)
        self.stop()
        
class MoveToTable(AdAction):
    
    def __init__(self, deliverGoal):
        AdAction.__init__(self)
        self._desired = ArActionDesired()
        self._deliverGoal=deliverGoal
        self._graph = [{1, 3, 4}, {0, 2, 4}, {1, 3}, {0, 2, 4}, {0, 1, 3, 5}, {0, 1, 3, 4, 6},{0}]
        self._path = [0,1,2,3,4,5,6]
                           
    def on_set_goals(self, goals):
        self._goals = goals
    
    def stop(self):
        AdAction.stop(self)
        
    def start(self):
        AdAction.start(self)
        #self.camera = getRobotInstance().arRobot.getPTZ()
        #self.camera.init();
        #self.camera.pan(-90);
        pathTask = getRobotInstance().pathTask   
        self._node = 4
        self._tmpGoal = self._goals[self._path[self._node]]
        pathTask.pathPlanToPose(extract_pose(self._tmpGoal),False)
        
    def fire(self, currentDesired):
        pathTask = getRobotInstance().pathTask
        if self._node==4:
            self._node=-1
        if(pathTask.getState() == ArPathPlanningTask.REACHED_GOAL):
            if(self._node<self._deliverGoal):
                print("REACHED_GOAL", self._node)
                self._node += 1
                self._tmpGoal = self._goals[self._path[self._node]]
                pathTask.pathPlanToPose(extract_pose(self._tmpGoal), False)
                print('path: ',self._path,'node: ',self._node)

            else:
                if self._deliverGoal!=0:
                    self.signal('TURN')
                else:
                    self.signal('noTurn')
                self.stop()
                