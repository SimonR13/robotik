'''
Created on 20.02.2014

@author: SR
'''
from sys import stderr
from AriaPy import ArPose, ArMapObject
from BaseArnlPy import ArPathPlanningTask
from aria_python.adaction import AdAction
from aria_python.robot import getRobotInstance

## Drives the robot to a given position. The action uses the ARNL functionality ArPathPlanningTask, which uses a scalar potential field.
#   
#  This action should not be used parallel with other movement actions.
class MoveToBar(AdAction):
    
    ## The constructor.
    def __init__(self):
        AdAction.__init__(self, "PathPlanning")
        self._goal = ArPose()
        self._state = "NONE"
        self._nextGoal = None
    
    ## Sets the position the robot should go to.
    # @param goal The goal the robot should go to.    
    def on_moveToCounter(self, goal):
#         if self._nextGoal is not None:
#             if self._nextGoal.getName() == "5" or  goal.getName() == "0":
#                 self.stop()
#         else:
        self._nextGoal = goal
        self._state = "IDLE"
    
    ## This signal is fired when the pathplanningTask produces an error.
    #  @param The error state.    
    def sig_error(self, state):
        self.signal("error", state)
        
    def start(self):
        self._state = "IDLE"
        AdAction.start(self)
        
    ## Drives the robot to a given position.
    #  @see ArAction.fire in ARIA-Reference API-Documentation.
    def fire(self, currentDesired):
        if self._state == "IDLE":
            if self._nextGoal is not None:
                pose = self._nextGoal
                self._nextGoal = None
                self._start = self.getRobot().getPose()
                if ArMapObject == type(pose):
                    print("going to", pose.getName())
                    pose = pose.getPose()
                if ArPose == type(pose):
                    print("going to Pose at: ({0}, {1}, {2})".format(pose.getX(), pose.getY(), pose.getTh()))
                else:
                    print("Error: invalid Object! It has to be of type ArPose or ArMapObject.", file=stderr)
                    return
                self._goal = pose
                self._state = "MOVE"
        elif self._state == "MOVE":
            getRobotInstance().pathTask.pathPlanToPose(self._goal,False)
            self._state = "MOVING"
        elif self._state == "MOVING":
            path_task = getRobotInstance().pathTask
            state = path_task.getState();
            #NOT_INITIALIZED        Task not initialized.
            #PLANNING_PATH          Planning the initial path.
            #MOVING_TO_GOAL         Moving to the goal.
            #REACHED_GOAL           Reached the goal.
            #FAILED_PLAN            Failed to plan a path to goal.
            #FAILED_MOVE            Failed to reach goal after plan obtained.
            #ABORTED_PATHPLAN       Aborted plan before done.
            #INVALID                Invalid state.
            if (ArPathPlanningTask.NOT_INITIALIZED == state):
                print("NOT_INITIALIZED")
                self.sig_error(state) 
                self._state = "IDLE"
            elif (ArPathPlanningTask.PLANNING_PATH == state):
                print("PLANNING_PATH")
            elif (ArPathPlanningTask.MOVING_TO_GOAL == state):
                print("MOVING_TO_GOAL")
            elif (ArPathPlanningTask.REACHED_GOAL == state):
                print("REACHED_GOAL")
                self._state = "IDLE"
                self.stop()
#                 self.signal("goal_reached")
            elif (ArPathPlanningTask.FAILED_PLAN == state):
                print("FAILED_PLAN")
                self.sig_error(state) 
                self._state = "IDLE"         
            elif (ArPathPlanningTask.FAILED_MOVE == state):
                print("FAILED_MOVE")
                self.sig_error(state)
                self._state = "IDLE"
            elif (ArPathPlanningTask.ABORTED_PATHPLAN == state):
                print("ABORTED_PATHPLAN")
                self.sig_error(state)
                self._state = "IDLE"
            elif (ArPathPlanningTask.INVALID == state):
                print("INVALID")
                self.sig_error(state)
                self._state = "IDLE"
            else:
                print("UNKNOWN_STATE", file=stderr)
                self.stop()
        else:
            print("ERROR: ", self._state)
            self.stop()


class CheckBar(AdAction):
    
    def __init__(self):
        AdAction.__init__(self)
        
    def on_checkBar(self, channel):
        self._channel = channel
        AdAction.start(self)
        
    def fire(self, desired):
        print("TODO:CheckBar()")