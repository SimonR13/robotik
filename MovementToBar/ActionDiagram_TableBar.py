# Data: {"nodes":[{"id":77,"location":{"y":80,"x":60},"type":"start"},{"id":81,"module":"MovementToBar/GripperMove.py","instanceName":"actionmove","location":{"y":60,"x":720},"priority":50,"actionName":"ActionMove","parameters":{},"type":"action"},{"id":82,"module":"de/trier/directaction/turn.py","instanceName":"turnLeft","location":{"y":200,"x":700},"priority":50,"actionName":"Turn","parameters":{"angle":"90"},"type":"action"},{"id":83,"module":"JPS/ObjectTrigger.py","instanceName":"objectdetectionbar","location":{"y":200,"x":840},"priority":50,"actionName":"ObjectDetectionBar","parameters":{"brennweite":"882","abstand":"75","channel":"1"},"type":"action"},{"id":84,"module":"de/trier/behaviour/movement/forward.py","instanceName":"forward","location":{"y":200,"x":1020},"priority":50,"actionName":"Forward","parameters":{"speed":"50"},"type":"action"},{"id":86,"module":"MovementToBar/PathPlan.py","instanceName":"actiongotobar","location":{"y":40,"x":180},"priority":50,"actionName":"ActionGoToBar","parameters":{"node":"1"},"type":"action"},{"id":108,"module":"gripper/GripperDrop.py","instanceName":"actiondrop","location":{"y":40,"x":420},"priority":50,"actionName":"ActionDrop","parameters":{},"type":"action"},{"id":109,"module":"gripper/GripperControl.py","instanceName":"grippercontrol","location":{"y":320,"x":400},"priority":50,"actionName":"GripperControl","parameters":{},"type":"action"}],"connections":[{"slotNode":81,"signal":{"normalParams":1,"name":"GoBack","namedParams":[]},"bendpoints":[],"slot":{"parametersOptional":[],"name":"on_goDistance","parametersRequired":["dist"]},"signalNode":108},{"slotNode":81,"signal":{"normalParams":1,"name":"GoForward","namedParams":[]},"bendpoints":[],"slot":{"parametersOptional":[],"name":"on_goDistance","parametersRequired":["dist"]},"signalNode":108},{"slotNode":82,"signal":{"normalParams":0,"name":"stopped","namedParams":[]},"bendpoints":[],"slot":{"parametersOptional":[],"name":"start","parametersRequired":[]},"signalNode":108},{"slotNode":83,"signal":{"normalParams":0,"name":"started","namedParams":[]},"bendpoints":[],"slot":{"parametersOptional":[],"name":"start","parametersRequired":[]},"signalNode":82},{"slotNode":84,"signal":{"normalParams":0,"name":"started","namedParams":[]},"bendpoints":[],"slot":{"parametersOptional":[],"name":"start","parametersRequired":[]},"signalNode":83},{"slotNode":84,"signal":{"normalParams":0,"name":"stopped","namedParams":[]},"bendpoints":[],"slot":{"parametersOptional":[],"name":"stop","parametersRequired":[]},"signalNode":83},{"slotNode":86,"signal":{"normalParams":0,"name":"start","namedParams":[]},"bendpoints":[],"slot":{"parametersOptional":[],"name":"start","parametersRequired":[]},"signalNode":77},{"slotNode":108,"signal":{"normalParams":0,"name":"stopped","namedParams":[]},"bendpoints":[],"slot":{"parametersOptional":[],"name":"start","parametersRequired":[]},"signalNode":86},{"slotNode":108,"signal":{"normalParams":1,"name":"paddleState","namedParams":[]},"bendpoints":[],"slot":{"parametersOptional":[],"name":"on_paddleState","parametersRequired":["ps"]},"signalNode":109},{"slotNode":108,"signal":{"normalParams":0,"name":"gripper_stopped","namedParams":[]},"bendpoints":[],"slot":{"parametersOptional":[],"name":"on_gripperClosed","parametersRequired":[]},"signalNode":109},{"slotNode":108,"signal":{"normalParams":1,"name":"GripState","namedParams":[]},"bendpoints":[],"slot":{"parametersOptional":[],"name":"on_gripState","parametersRequired":["gs"]},"signalNode":109},{"slotNode":108,"signal":{"normalParams":0,"name":"Stop","namedParams":[]},"bendpoints":[],"slot":{"parametersOptional":[],"name":"on_afterMove","parametersRequired":[]},"signalNode":81},{"slotNode":109,"signal":{"normalParams":0,"name":"stopped","namedParams":[]},"bendpoints":[],"slot":{"parametersOptional":[],"name":"start","parametersRequired":[]},"signalNode":86},{"slotNode":109,"signal":{"normalParams":0,"name":"StoreGripper","namedParams":[]},"bendpoints":[],"slot":{"parametersOptional":[],"name":"on_gripStore","parametersRequired":[]},"signalNode":108},{"slotNode":109,"signal":{"normalParams":0,"name":"LowerGripper","namedParams":[]},"bendpoints":[],"slot":{"parametersOptional":[],"name":"on_lowerGripper","parametersRequired":[]},"signalNode":108},{"slotNode":109,"signal":{"normalParams":0,"name":"OpenGripper","namedParams":[]},"bendpoints":[],"slot":{"parametersOptional":[],"name":"on_openGripper","parametersRequired":[]},"signalNode":108}]}
from aria_python.robot import getRobotInstance, ExecuteActionsFlag
from pydispatch import dispatcher
import gripper.GripperDrop
import gripper.GripperControl
import MovementToBar.PathPlan
import JPS.ObjectTrigger
import de.trier.directaction.turn
import de.trier.behaviour.movement.forward
import MovementToBar.GripperMove

if ExecuteActionsFlag:
    robot = getRobotInstance()
    action_actionmove = MovementToBar.GripperMove.ActionMove()
    action_actionmove.__action_name__ = 'actionmove'
    action_actionmove.__diagram_name__ = 'MovementToBar/ActionDiagram_TableBar.py'
    robot.addAction(action_actionmove, 50)

    action_turnLeft = de.trier.directaction.turn.Turn(angle = 90)
    action_turnLeft.__action_name__ = 'turnLeft'
    action_turnLeft.__diagram_name__ = 'MovementToBar/ActionDiagram_TableBar.py'
    robot.addAction(action_turnLeft, 50)

    action_objectdetectionbar = JPS.ObjectTrigger.ObjectDetectionBar(brennweite = 882, abstand = 75, channel = 1)
    action_objectdetectionbar.__action_name__ = 'objectdetectionbar'
    action_objectdetectionbar.__diagram_name__ = 'MovementToBar/ActionDiagram_TableBar.py'
    robot.addAction(action_objectdetectionbar, 50)

    action_forward = de.trier.behaviour.movement.forward.Forward(speed = 50)
    action_forward.__action_name__ = 'forward'
    action_forward.__diagram_name__ = 'MovementToBar/ActionDiagram_TableBar.py'
    robot.addAction(action_forward, 50)

    action_actiongotobar = MovementToBar.PathPlan.ActionGoToBar(node = 1)
    action_actiongotobar.__action_name__ = 'actiongotobar'
    action_actiongotobar.__diagram_name__ = 'MovementToBar/ActionDiagram_TableBar.py'
    robot.addAction(action_actiongotobar, 50)

    action_actiondrop = gripper.GripperDrop.ActionDrop()
    action_actiondrop.__action_name__ = 'actiondrop'
    action_actiondrop.__diagram_name__ = 'MovementToBar/ActionDiagram_TableBar.py'
    robot.addAction(action_actiondrop, 50)

    action_grippercontrol = gripper.GripperControl.GripperControl()
    action_grippercontrol.__action_name__ = 'grippercontrol'
    action_grippercontrol.__diagram_name__ = 'MovementToBar/ActionDiagram_TableBar.py'
    robot.addAction(action_grippercontrol, 50)

    dispatcher.connect(action_actionmove.on_goDistance, sender=action_actiondrop, signal='GoBack')
    dispatcher.connect(action_actionmove.on_goDistance, sender=action_actiondrop, signal='GoForward')
    dispatcher.connect(action_turnLeft.start, sender=action_actiondrop, signal='stopped')
    dispatcher.connect(action_objectdetectionbar.start, sender=action_turnLeft, signal='started')
    dispatcher.connect(action_forward.start, sender=action_objectdetectionbar, signal='started')
    dispatcher.connect(action_forward.stop, sender=action_objectdetectionbar, signal='stopped')
    dispatcher.connect(action_actiondrop.start, sender=action_actiongotobar, signal='stopped')
    dispatcher.connect(action_actiondrop.on_paddleState, sender=action_grippercontrol, signal='paddleState')
    dispatcher.connect(action_actiondrop.on_gripperClosed, sender=action_grippercontrol, signal='gripper_stopped')
    dispatcher.connect(action_actiondrop.on_gripState, sender=action_grippercontrol, signal='GripState')
    dispatcher.connect(action_actiondrop.on_afterMove, sender=action_actionmove, signal='Stop')
    dispatcher.connect(action_grippercontrol.start, sender=action_actiongotobar, signal='stopped')
    dispatcher.connect(action_grippercontrol.on_gripStore, sender=action_actiondrop, signal='StoreGripper')
    dispatcher.connect(action_grippercontrol.on_lowerGripper, sender=action_actiondrop, signal='LowerGripper')
    dispatcher.connect(action_grippercontrol.on_openGripper, sender=action_actiondrop, signal='OpenGripper')
    action_actiongotobar.start()
