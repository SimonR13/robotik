'''
Created on 19.02.2014

@author: SR
'''
from AriaPy import ArGripper
from AriaPy import ArActionDesired
from aria_python.adaction import AdAction
import time

class ActionDrop(AdAction):
    
    def __init__(self):
        AdAction.__init__(self)
        self._desired = ArActionDesired()
        self._gripper = ArGripper(self.getRobot())
        self._dist = 500
        
    def start(self):
        AdAction.start(self)
        self.sig_goForward(self._dist)
        self._gripState = 0
        self._afterMove = False
        self._gripperClosed = True
        self._gripperMoving = False
        self._dropped = False
        self._firstRun = True
            
    def sig_goBack(self, dist):
        self.signal("GoBack", dist)
         
    def sig_goForward(self, dist):
        self.signal("GoForward", dist)
         
    def sig_openGripper(self):
        self.signal("OpenGripper")
         
    def sig_lowerGripper(self):
        self.signal("LowerGripper")
        
    def sig_storeGripper(self):
        self.signal("StoreGripper")
        
    def sig_droppedEmpty(self):
        self.signal("DroppedEmpty")
         
    def on_paddleState(self, ps):
        self._paddleState = ps
        
    def on_gripState(self, gs):
        self._gripState = gs
            
    def on_gripperClosed(self):
        if (self._gripperClosed):
            self._gripperClosed = False
        elif (not self._gripperClosed) & (self._paddleState > 0):
            self._gripperClosed = True
#         print("on_gripperClosed", self._gripperClosed)
            
    def on_afterMove(self):
        self._afterMove = True
        
    def fire(self, currentDesired):
        if (self._afterMove == True) & (self._dropped == False):
            self.sig_lowerGripper()
            time.sleep(2)
            self.sig_openGripper()
            if not self._gripper.isGripMoving():
                self._dropped = True
                self.sig_droppedEmpty()
            
        print("gripState", self._gripState, "afterMove", self._afterMove, "dropped", self._dropped)
        if (self._afterMove == True) & (self._dropped == True):
            self.sig_goBack(self._dist * (-1))
            time.sleep(3)
            self.sig_storeGripper()
            if not self._gripper.isGripMoving():
                self._afterMove = False
                self._dropped = False
            
        if (not self._dropped) & (not self._afterMove) & (not self._firstRun):
            self.sig_droppedEmpty()
            self.stop()
            self._firstRun = False
    