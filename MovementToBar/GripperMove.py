'''
Created on 20.02.2014

@author: SR
'''

from aria_python.adaction import AdAction
from de.trier.function.directaction import move
from de.trier.function.utility import has_stopped

class ActionMove(AdAction):
    def __init__(self, dist=500):
        AdAction.__init__(self)
        self._distance = dist
        
    def start(self):
        AdAction.start(self)
        move(self._distance)
        
    def on_goDistance(self, dist):
        self._distance = dist
        self.start()
        
    def sig_stop(self):
        self.signal("Stop")
        AdAction.stop(self)

    def fire (self, currentDesired):
        if has_stopped():
            print("ActionMove.stop()")
            self.sig_stop()
