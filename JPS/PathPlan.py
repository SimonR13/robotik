from AriaPy import ArActionDesired
from AriaPy import ArMapObject
from aria_python.robot import getRobotInstance
from aria_python.adaction import AdAction
from de.trier.function.utility import extract_pose
from BaseArnlPy import ArPathPlanningTask


class initClass(AdAction):

    def __init__(self):
        AdAction.__init__(self)
        self._desired = ArActionDesired()
        self.initPath()
        self._goals=self.getGoals()
        
    def getGoals(self):
        # Get PathTask
        path_task=getRobotInstance().pathTask
        # Get Map
        ar_map=path_task.getAriaMap()
        # Get Tuple of Goals
        self.goals = sorted(ar_map.findMapObjectsOfType("Goal") + \
                        ar_map.findMapObjectsOfType("GoalWithHeading"), key = ArMapObject.getName, reverse = False)
        return self.goals
        
    def initPath(self):
        # Get PathTask
#         path_task=getRobotInstance().pathTask
        # Get Map
        #ar_map=path_task.getAriaMap()
        #self._lines = get_lines(ar_map)
        self._graph = [{1, 7}, {0, 2}, {1, 3}, {2, 4}, {3, 5}, {4, 6}, {5, 7}, {0, 6}]
        self._path = [0,1,2,3]
        print(self._graph, self._path)
        
    def fire(self, currentDesired):
        self.signal("graph", self._graph)
        self.signal("goals", self._goals)
        self.stop()
        
class ActionGoTable(AdAction):
    
    def __init__(self):
        AdAction.__init__(self)
        self._desired = ArActionDesired()
        self._path = [0,1]
                           
    def on_set_goals(self, goals):
        self._goals = goals
    
    def on_set_node(self,node):
        self._node=node
        
    def on_set_graph(self, graph):
        self._graph = graph
        
    def sig_detect(self):
        self.signal("Detect")
        
    def stop(self):
        self.signal("node", self._node) # Naechster knoten
        AdAction.stop(self)
        
    def start(self):
        AdAction.start(self)
        #self.camera = getRobotInstance().arRobot.getPTZ()
        #self.camera.init();
        #self.camera.pan(-90);
        pathTask = getRobotInstance().pathTask      
        self._node = 0
        self._tmpGoal = self._goals[self._path[self._node]]
        pathTask.pathPlanToPose(extract_pose(self._tmpGoal),False)
        
    def fire(self, currentDesired):
        pathTask = getRobotInstance().pathTask    
        if(pathTask.getState() == ArPathPlanningTask.REACHED_GOAL):
            if(self._node<len(self._path)-1):
                print("REACHED_GOAL", self._node)
                self._node += 1
                self._tmpGoal = self._goals[self._path[self._node]]
                pathTask.pathPlanToPose(extract_pose(self._tmpGoal), False)
                    
#                 print("self._node % 2", self._node % 2)
#                 if((self._node % 2) == 0):
#                     print("self.sig_detect()")
#                     self.sig_detect()
                print('path: ',self._path,'node: ',self._node)

            else:
                self.stop()
                
class ActionGoToBar(AdAction):
    
    def __init__(self):
        AdAction.__init__(self)
        self._desired = ArActionDesired()
        self._path = [0,1,2,3]

    def on_set_node(self,node):
        self._node=node-1    
        
    def start(self):
        AdAction.start(self)
        pathTask = getRobotInstance().pathTask
        self._tmpGoal = self._goals[self._path[self._node]]
        pathTask.pathPlanToPose(extract_pose(self._tmpGoal),False)
    
    def fire(self, currentDesired):
        pathTask = getRobotInstance().pathTask        
        if(pathTask.getState() == ArPathPlanningTask.REACHED_GOAL):
            if(self._node==4):
                self.stop()
            else:
                if(self._node<len(self._path)-1):
                    print("REACHED_GOAL")
                    if(self._node==0):
                        self._node=4
                        pathTask.pathPlanToPose(extract_pose(self._goals[self._path[self._node]]), False)
                    else:
                        self._node += 1
                        self._tmpGoal = self._goals[self._path[self._node]]
                        pathTask.pathPlanToPose(extract_pose(self._tmpGoal), False)
                        print('path: ',self._path,'node: ',self._node)                    
    
                else:
                    self._node = -1

class ActionGoBar(AdAction):
    
    def __init__(self):
        AdAction.__init__(self)
        self._desired = ArActionDesired()
        self._node = 5
        
    def start(self):
        AdAction.start(self)
        pathTask = getRobotInstance().pathTask
        pathTask.pathPlanToPose(extract_pose(self._goals[self._path[self._node]]),False)    
        