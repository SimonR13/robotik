import math
from aria_python.adaction import AdAction
from AriaPy import ArActionDesired
from aria_python.robot import getRobotInstance
import time

class Timer(AdAction):

    def __init__(self, time=5):
        AdAction.__init__(self)
        self._time = time
        self._start = False
        
    def on_startTimer(self):
        self._start =True
        
    def stop(self):
        AdAction.stop(self)
        self._start = False    
    
    def fire(self, currentDesired):
        if self._start:
            print("Timer start. Time =", self._time)
            time.sleep(self._time)
            print("Timer stopt.")
            self.stop()
            
class MoveForward(AdAction):
    
    ## The Constructor
    #  @param speed The speed the robot shall accelerate to.
    def __init__(self,speed=200):
        AdAction.__init__(self, "Forward")
        self._desired = ArActionDesired()
        self._speed = speed
        
    def start(self):
        AdAction.start(self)
        self._distance = 2000
        self._rob = getRobotInstance().arRobot
        self.startX = self._rob.getPose().getX()
        self.startY = self._rob.getPose().getY() 
        

    ## Desires to accelerate to the given speed.
    #  @see ArAction.fire in ARIA-Reference API-Documentation.                  
    def fire(self, currentDesired):
        self._desired.reset()
        if not self.move(self.startX, self.startY):
            self._desired.setVel(0)
            self.stop()
        else:
            self._desired.setVel(self._speed)
        return self._desired
    
    ## Slot to set the speed.
    #  @param speed The speed the robot shall accelerate to.
    def on_set_speed(self, speed):
        self._speed = speed
        
    def move(self, startX, startY):
        X = startX - self._rob.getPose().getX()
        Y = startY - self._rob.getPose().getY()
        dist = math.sqrt((X - Y)**2 + (startX - startY)**2)
        if (self._distance == 2000):
            self._distance += dist
        print("dist", dist, "distance", self._distance)
        if (dist < self._distance):
            return True
        
        return False