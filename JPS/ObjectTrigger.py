from AriaPy import ArACTS_1_2, ArACTSBlob
from aria_python.adaction import AdAction

class ObjectDetectionTable(AdAction):

    def __init__(self,abstand,brennweite):
        AdAction.__init__(self)
        self._acts = ArACTS_1_2() # ACTS Objekt anlegen
        self.abstand=abstand
        self.brennweite=brennweite
    
    def start(self):
        if self._acts.openPort(None): # Verbindung zu ACTS herstellen
            AdAction.start(self)
#             print("ObjectDetectionTable.start()")
            
    def fire(self, currentDesired):
#         print("ObjectDetectionTable.fire()")
        if self._acts.requestPacket(): # Pakete von ACTS anfordern
#             print("Paket erhalten")
            if self._acts.receiveBlobInfo(): # Bildinformationen anfordern
                print("Bildinformation erhalten")
                for channel in range(1,5):
                    self.print_channel(channel)
        
    def stop(self):
        if self._acts.closePort(): # Verbindung zu ACTS beenden
            AdAction.stop(self)
                    
    def print_channel(self, channel):
        print("Channel:",channel)
        num_blobs = self._acts.getNumBlobs(channel)
        for blob_num in range(1,num_blobs+1):
            self.distanz_berechnung(channel, blob_num)
    
    def print_blob_info(self, channel, blob_num):
        blob = ArACTSBlob()
        if self._acts.getBlob(channel,blob_num,blob): # Blob Info auslesen
            print(" Blob:",blob_num)
            print(" Area:",blob.getArea()) # Flaeche in Pixeln
            # Mittelpunkt des Blobs
            print(" CG: ({0}, {1})".format(blob.getXCG(),blob.getYCG()))
            # Boundingbox des Blobs
            print(" Bounding Box: ({0}, {1}, {2}, {3})"
            .format(blob.getLeft(),blob.getTop(),
            blob.getRight(),blob.getBottom()))

    def distanz_berechnung(self, channel,num_blobs):
#         breite_leer = 11
        #breite_voll=7,5
        blob = ArACTSBlob()
        if self._acts.getBlob(channel,num_blobs,blob): #leer 1.57 0,63 #voll 2 
            
            #Pixelbreite des Objektes im Bild
            breite= blob.getRight() - blob.getLeft()
            hoehe= blob.getBottom() - blob.getTop()
            verhaeltniss=hoehe/breite
            print('Hoehe: ',hoehe,' Breite: ',breite,' Verhaeltniss: ', verhaeltniss)
            #Berechnung der tatsaechlichen Groesse
            #breite_tatsaechlich = (breite*self.abstand)/ self.brennweite
            print(' Area: ',blob.getArea(), 'Position X', blob.getXCG())
            
            #Distanz ausrechnen
            if (blob.getArea()>5000) & (blob.getArea() < 7500):
                if (verhaeltniss<0.7) & (verhaeltniss>0.6):
                    if(blob.getXCG() > 240) & (blob.getXCG() < 450):
                        if (blob.getYCG() > 350 & blob.getYCG()<370):
                            self.signal("channel", channel) # Welche Farbe wurde vom Tisch genommen
                            self.stop()
                        
class ObjectDetectionBar(AdAction):
    
    def __init__(self,abstand,brennweite, channel=1):
        AdAction.__init__(self)
        self._acts = ArACTS_1_2() # ACTS Objekt anlegen
        self.abstand=abstand
        self.brennweite=brennweite
        self.channel = channel
        
    def start(self):
        if self._acts.openPort(None): # Verbindung zu ACTS herstellen
            AdAction.start(self)
    
    def stop(self):
        if self._acts.closePort(): # Verbindung zu ACTS beenden
            AdAction.stop(self)

    def on_set_channel(self,channel):
        self.channel=channel
    
    def fire(self, currentDesired):
        if self._acts.requestPacket(): # Pakete von ACTS anfordern
            if self._acts.receiveBlobInfo(): # Bildinformationen anfordern
                self.print_channel(self.channel)
    
    def print_channel(self, channel):
        print("Channel:",channel)
        num_blobs = self._acts.getNumBlobs(channel)
        for blob_num in range(1,num_blobs+1):
            self.distanz_berechnung(channel, blob_num)
    
    def LookingForFull(self, channel, num_blobs):
        breite_leer = 11
        blob = ArACTSBlob()
        if self._acts.getBlob(channel,num_blobs,blob):
            #Pixelbreite des Objektes im Bild
            breite= blob.getRight() - blob.getLeft()
            #Berechnung der tatsaechlichen Groesse
            breite_tatsaechlich = (breite*self.abstand)/ self.brennweite
            print('Breite PX: ',breite,'Breite W: ',breite_tatsaechlich)
            #Distanz ausrechnen
            if(blob.getArea()>1000):
                if ((breite_tatsaechlich < breite_leer-1)) :
                    if(blob.getXCG()>250):
                        self.signal("channel", channel) # Welche Farbe wurde vom Tisch genommen
                        self.stop()