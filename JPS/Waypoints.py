# Imports from ARIA
from AriaPy import ArMapObject

# Imports from ARIA-Wrapper
from aria_python.adaction import AdFunction
from aria_python.robot import getRobotInstance

## Makes the waypoints contained in the current Map accesible.
class Waypoints(AdFunction):

    ## The constructor.
    def __init__(self):
        AdFunction.__init__(self, "WaypointCollection")
        
        # Get PathTask
        path_task = getRobotInstance().pathTask
        # Get Map
        ar_map = path_task.getAriaMap()
        # Get Tuple of Goals
        self._goals = sorted(ar_map.findMapObjectsOfType("Goal")+ar_map.findMapObjectsOfType("GoalWithHeading"),
                             key=ArMapObject.getName)
        
        self._current_goal = 0
        
        self._pickedUp = False
        
    ## This Signal sends the waypoint with the given index.
    # @param index The index of the waypoint that is requested.
    def _sig_waypoint(self, index):
#         print('Index: ',index)
        if self._pickedUp:
            if index<5:
                self.signal("waypoint", self._goals[index])
                self._sig_waypoint_no(index)
            else:
                self.stop()
        else:
            if index<4:    
                self.signal("waypoint", self._goals[index])
                self._sig_waypoint_no(index)
            else:
                self.on_get_waypoint(0)
    
    ## This Signal sends the index of the waypoint.
    # @param index The index of the waypoint that is requested.
    def _sig_waypoint_no(self, index):
        self.signal("waypoint_no", index)
    
    ## Sends an error message when an non existing index is requested.
    # @param message The message that is sent.
    def _sig_error(self, message):
        self.signal("error", message)
    
    ## This signal is triggered when the end of the collection is reached.
    def _sig_end_of_list(self):
        self.signal("end_of_list")

    def _increment(self):
        self._current_goal += 1
        if self._current_goal >= len(self._goals):
            self._current_goal = 0
            self._sig_end_of_list()
            
    def _decrement(self):
        self._current_goal -= 1
        if self._current_goal < 0:
            self._current_goal = max(0, len(self._goals) - 1)
            self._sig_end_of_list()

    ## Triggers the collection to send the next waypoint.    
    def on_get_next(self):
        self._increment()
        self.on_get_current()
        
    def on_get_pickedUp(self):
        self._pickedUp=True
        self._decrement()
#         self.on_get_next()
        
    
    ## Triggers the collection to send the current waypoint.    
    def on_get_current(self):
        self.on_get_waypoint(self._current_goal)

    ## Triggers the collection to send the waypoint requestet by the prarmeter index.
    #  @param index The index of the required waypoint.
    def on_get_waypoint(self, index):
        num_wps = len(self._goals)
        if index < 0 or index >= num_wps:
            self._sig_error("Index {0} out of Range [0, {1}[".format(index, num_wps))
        else:
            self._sig_waypoint(index)   

#===============================================================================
# class WaypointsToBar(AdFunction):
#     
#     def __init__(self):    
#         AdFunction.__init__(self, "WaypointToBarCollection")
#         
#         # Get PathTask
# #         path_task = getRobotInstance().pathTask
#         # Get Map
# #         ar_map = path_task.getAriaMap()
# #         # Get Tuple of Goals
# #         self._goals = None
# #                         sorted(ar_map.findMapObjectsOfType("Goal")+ar_map.findMapObjectsOfType("GoalWithHeading"),
# #                             key=ArMapObject.getName)
#         
#         self._current_goal = 0
#         self._lastIndex = 0
#         self._pickedUp = False
#         
#     def _sig_waypoint(self, index):
#         self.signal("waypoint", self._goals[index])
#         self._sig_waypoint_no(index)
#     
#     def _sig_waypoint_no(self, index):
#         self.signal("waypoint_no", index)
#     
#     def _sig_error(self, message):
#         self.signal("error", message)
#     
#     def _sig_end_of_list(self):
#         self.signal("end_of_list")
# 
#     def _increment(self):
#         self._current_goal += 1
#         if self._current_goal >= len(self._goals):
#             self._current_goal = 0
#             self._sig_end_of_list()
#             
#     def _decrement(self):
#         self._current_goal -= 1
#         if self._current_goal < 0:
#             self._current_goal = max(0, len(self._goals) - 1)
#             self._sig_end_of_list()
# 
#     def on_get_next(self):
#         self._increment()
#         self.on_get_current()
#         
#     def on_get_pickedUp(self):
#         self._pickedUp=True
#         
#     def on_set_lastGoal(self, goal):
#         self._current_goal = goal 
# 
#     def on_get_goals(self, goals):
#         self._goals = goals
# 
#     def on_get_current(self):
#         self.on_get_waypoint(self._current_goal)
# 
#     def on_get_waypoint(self, index):
#         num_wps = len(self._goals)
#         if index < 0 or index >= num_wps:
#             self._sig_error("Index {0} out of Range [0, {1}[".format(index, num_wps))
#         else:
#             self._sig_waypoint(index)
#===============================================================================
        