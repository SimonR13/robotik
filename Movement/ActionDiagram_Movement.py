# Data: {"nodes":[{"id":38,"location":{"y":0,"x":25},"type":"start"},{"id":39,"module":"Movement/MovementTable.py","instanceName":"checktable","location":{"y":50,"x":175},"priority":50,"actionName":"CheckTable","parameters":{},"type":"action"},{"id":40,"module":"de/trier/directaction/turn.py","instanceName":"turnRight","location":{"y":350,"x":325},"priority":50,"actionName":"Turn","parameters":{"angle":"-90"},"type":"action"},{"id":41,"module":"JPS/Waypoints.py","instanceName":"waypoints","location":{"y":25,"x":575},"priority":50,"actionName":"Waypoints","parameters":{},"type":"action"},{"id":42,"module":"JPS/ObjectTrigger.py","instanceName":"objectdetectiontable","location":{"y":350,"x":75},"priority":50,"actionName":"ObjectDetectionTable","parameters":{"brennweite":"882","abstand":"65"},"type":"action"},{"id":43,"module":"JPS/Waypoints.py","instanceName":"waypointsToBar","location":{"y":250,"x":875},"priority":50,"actionName":"Waypoints","parameters":{},"type":"action"},{"id":44,"module":"de/trier/behaviour/movement/simple_go_to.py","instanceName":"simplegoto","location":{"y":550,"x":900},"priority":50,"actionName":"SimpleGoTo","parameters":{},"type":"action"},{"id":45,"module":"gripper/GripperPickUp.py","instanceName":"actionpickup","location":{"y":475,"x":525},"priority":50,"actionName":"ActionPickUp","parameters":{},"type":"action"},{"id":46,"module":"gripper/GripperControl.py","instanceName":"grippercontrol","location":{"y":575,"x":175},"priority":50,"actionName":"GripperControl","parameters":{},"type":"action"}],"connections":[{"slotNode":39,"signal":{"normalParams":1,"name":"waypoint","namedParams":[]},"bendpoints":[{"y":150,"x":825},{"y":300,"x":825},{"y":300,"x":450},{"y":325,"x":450},{"y":325,"x":325},{"y":325,"x":25},{"y":175,"x":25}],"slot":{"parametersOptional":[],"name":"on_go_to","parametersRequired":["goal"]},"signalNode":41},{"slotNode":39,"signal":{"normalParams":0,"name":"stopped","namedParams":[]},"bendpoints":[{"y":425,"x":250},{"y":300,"x":250},{"y":300,"x":100},{"y":150,"x":100}],"slot":{"parametersOptional":[],"name":"stop","parametersRequired":[]},"signalNode":42},{"slotNode":39,"signal":{"normalParams":0,"name":"start","namedParams":[]},"bendpoints":[{"y":50,"x":150},{"y":125,"x":150}],"slot":{"parametersOptional":[],"name":"start","parametersRequired":[]},"signalNode":38},{"slotNode":40,"signal":{"normalParams":0,"name":"stopped","namedParams":[]},"bendpoints":[{"y":450,"x":250},{"y":450,"x":275},{"y":425,"x":275}],"slot":{"parametersOptional":[],"name":"start","parametersRequired":[]},"signalNode":42},{"slotNode":41,"signal":{"normalParams":0,"name":"goal_reached","namedParams":[]},"bendpoints":[{"y":200,"x":450},{"y":125,"x":450}],"slot":{"parametersOptional":[],"name":"on_get_next","parametersRequired":[]},"signalNode":39},{"slotNode":41,"signal":{"normalParams":0,"name":"start","namedParams":[]},"bendpoints":[{"y":25,"x":425},{"y":100,"x":425}],"slot":{"parametersOptional":[],"name":"on_get_current","parametersRequired":[]},"signalNode":38},{"slotNode":41,"signal":{"normalParams":0,"name":"stopped","namedParams":[]},"bendpoints":[{"y":575,"x":825},{"y":450,"x":825},{"y":450,"x":525},{"y":150,"x":525}],"slot":{"parametersOptional":[],"name":"on_get_pickedUp","parametersRequired":[]},"signalNode":45},{"slotNode":42,"signal":{"normalParams":0,"name":"DETECT","namedParams":[]},"bendpoints":[{"y":175,"x":400},{"y":275,"x":400},{"y":275,"x":50},{"y":425,"x":50}],"slot":{"parametersOptional":[],"name":"start","parametersRequired":[]},"signalNode":39},{"slotNode":43,"signal":{"normalParams":1,"name":"waypoint_no","namedParams":[]},"bendpoints":[{"y":175,"x":850},{"y":400,"x":850}],"slot":{"parametersOptional":[],"name":"on_get_waypoint","parametersRequired":["index"]},"signalNode":41},{"slotNode":43,"signal":{"normalParams":0,"name":"goal_reached","namedParams":[]},"bendpoints":[{"y":675,"x":1100},{"y":475,"x":1100},{"y":475,"x":850},{"y":425,"x":850},{"y":425,"x":800},{"y":350,"x":800}],"slot":{"parametersOptional":[],"name":"on_get_next","parametersRequired":[]},"signalNode":44},{"slotNode":43,"signal":{"normalParams":0,"name":"stopped","namedParams":[]},"bendpoints":[{"y":575,"x":825},{"y":450,"x":825},{"y":450,"x":775},{"y":325,"x":775}],"slot":{"parametersOptional":[],"name":"on_get_current","parametersRequired":[]},"signalNode":45},{"slotNode":44,"signal":{"normalParams":1,"name":"waypoint","namedParams":[]},"bendpoints":[{"y":375,"x":1125},{"y":725,"x":1125},{"y":725,"x":875},{"y":675,"x":875}],"slot":{"parametersOptional":[],"name":"on_go_to","parametersRequired":["goal"]},"signalNode":43},{"slotNode":44,"signal":{"normalParams":0,"name":"stopped","namedParams":[]},"bendpoints":[{"y":575,"x":875},{"y":625,"x":875}],"slot":{"parametersOptional":[],"name":"start","parametersRequired":[]},"signalNode":45},{"slotNode":45,"signal":{"normalParams":0,"name":"stopped","namedParams":[]},"bendpoints":[{"y":500,"x":500},{"y":550,"x":500}],"slot":{"parametersOptional":[],"name":"start","parametersRequired":[]},"signalNode":40},{"slotNode":45,"signal":{"normalParams":1,"name":"BreakBeamState","namedParams":[]},"bendpoints":[{"y":700,"x":450},{"y":600,"x":450}],"slot":{"parametersOptional":[],"name":"on_breakBeamState","parametersRequired":["bbs"]},"signalNode":46},{"slotNode":45,"signal":{"normalParams":1,"name":"paddleState","namedParams":[]},"bendpoints":[{"y":800,"x":475},{"y":650,"x":475}],"slot":{"parametersOptional":[],"name":"on_paddleStateChange","parametersRequired":["ps"]},"signalNode":46},{"slotNode":46,"signal":{"normalParams":0,"name":"stopped","namedParams":[]},"bendpoints":[{"y":525,"x":450},{"y":525,"x":75},{"y":650,"x":75}],"slot":{"parametersOptional":[],"name":"start","parametersRequired":[]},"signalNode":40},{"slotNode":46,"signal":{"normalParams":0,"name":"StoreGripper","namedParams":[]},"bendpoints":[{"y":725,"x":825},{"y":850,"x":825},{"y":850,"x":125},{"y":725,"x":125}],"slot":{"parametersOptional":[],"name":"on_gripStore","parametersRequired":[]},"signalNode":45},{"slotNode":46,"signal":{"normalParams":0,"name":"DeployGripper","namedParams":[]},"bendpoints":[{"y":600,"x":850},{"y":875,"x":850},{"y":875,"x":425},{"y":875,"x":75},{"y":700,"x":75}],"slot":{"parametersOptional":[],"name":"on_gripDeploy","parametersRequired":[]},"signalNode":45}]}
from aria_python.robot import getRobotInstance, ExecuteActionsFlag
from pydispatch import dispatcher
import gripper.GripperControl
import Movement.MovementTable
import JPS.ObjectTrigger
import de.trier.behaviour.movement.simple_go_to
import de.trier.directaction.turn
import gripper.GripperPickUp
import JPS.Waypoints

if ExecuteActionsFlag:
    robot = getRobotInstance()
    action_checktable = Movement.MovementTable.CheckTable()
    action_checktable.__action_name__ = 'checktable'
    action_checktable.__diagram_name__ = 'Movement/ActionDiagram_Movement.py'
    robot.addAction(action_checktable, 50)

    action_turnRight = de.trier.directaction.turn.Turn(angle = -90)
    action_turnRight.__action_name__ = 'turnRight'
    action_turnRight.__diagram_name__ = 'Movement/ActionDiagram_Movement.py'
    robot.addAction(action_turnRight, 50)

    action_waypoints = JPS.Waypoints.Waypoints()
    action_waypoints.__action_name__ = 'waypoints'
    action_waypoints.__diagram_name__ = 'Movement/ActionDiagram_Movement.py'
    robot.addAction(action_waypoints, 50)

    action_objectdetectiontable = JPS.ObjectTrigger.ObjectDetectionTable(brennweite = 882, abstand = 65)
    action_objectdetectiontable.__action_name__ = 'objectdetectiontable'
    action_objectdetectiontable.__diagram_name__ = 'Movement/ActionDiagram_Movement.py'
    robot.addAction(action_objectdetectiontable, 50)

    action_waypointsToBar = JPS.Waypoints.Waypoints()
    action_waypointsToBar.__action_name__ = 'waypointsToBar'
    action_waypointsToBar.__diagram_name__ = 'Movement/ActionDiagram_Movement.py'
    robot.addAction(action_waypointsToBar, 50)

    action_simplegoto = de.trier.behaviour.movement.simple_go_to.SimpleGoTo()
    action_simplegoto.__action_name__ = 'simplegoto'
    action_simplegoto.__diagram_name__ = 'Movement/ActionDiagram_Movement.py'
    robot.addAction(action_simplegoto, 50)

    action_actionpickup = gripper.GripperPickUp.ActionPickUp()
    action_actionpickup.__action_name__ = 'actionpickup'
    action_actionpickup.__diagram_name__ = 'Movement/ActionDiagram_Movement.py'
    robot.addAction(action_actionpickup, 50)

    action_grippercontrol = gripper.GripperControl.GripperControl()
    action_grippercontrol.__action_name__ = 'grippercontrol'
    action_grippercontrol.__diagram_name__ = 'Movement/ActionDiagram_Movement.py'
    robot.addAction(action_grippercontrol, 50)

    dispatcher.connect(action_checktable.on_go_to, sender=action_waypoints, signal='waypoint')
    dispatcher.connect(action_checktable.stop, sender=action_objectdetectiontable, signal='stopped')
    dispatcher.connect(action_turnRight.start, sender=action_objectdetectiontable, signal='stopped')
    dispatcher.connect(action_waypoints.on_get_next, sender=action_checktable, signal='goal_reached')
    dispatcher.connect(action_waypoints.on_get_pickedUp, sender=action_actionpickup, signal='stopped')
    dispatcher.connect(action_objectdetectiontable.start, sender=action_checktable, signal='DETECT')
    dispatcher.connect(action_waypointsToBar.on_get_waypoint, sender=action_waypoints, signal='waypoint_no')
    dispatcher.connect(action_waypointsToBar.on_get_next, sender=action_simplegoto, signal='goal_reached')
    dispatcher.connect(action_waypointsToBar.on_get_current, sender=action_actionpickup, signal='stopped')
    dispatcher.connect(action_simplegoto.on_go_to, sender=action_waypointsToBar, signal='waypoint')
    dispatcher.connect(action_simplegoto.start, sender=action_actionpickup, signal='stopped')
    dispatcher.connect(action_actionpickup.start, sender=action_turnRight, signal='stopped')
    dispatcher.connect(action_actionpickup.on_breakBeamState, sender=action_grippercontrol, signal='BreakBeamState')
    dispatcher.connect(action_actionpickup.on_paddleStateChange, sender=action_grippercontrol, signal='paddleState')
    dispatcher.connect(action_grippercontrol.start, sender=action_turnRight, signal='stopped')
    dispatcher.connect(action_grippercontrol.on_gripStore, sender=action_actionpickup, signal='StoreGripper')
    dispatcher.connect(action_grippercontrol.on_gripDeploy, sender=action_actionpickup, signal='DeployGripper')
    action_checktable.start()
    action_waypoints.on_get_current()
