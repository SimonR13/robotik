'''
Created on 19.02.2014

@author: SR
'''
from AriaPy import ArGripper
from AriaPy import ArActionDesired
from aria_python.adaction import AdAction
import time

class ActionDrop(AdAction):
    
    def __init__(self):
        AdAction.__init__(self)
        self._desired = ArActionDesired()
        self._gripper = ArGripper(self.getRobot())
        self._dist = 200
        
    def start(self):
        print('DROP')
        AdAction.start(self)
        self.sig_goForward(self._dist)
        self._gripState = 0
        self._afterMove = False
        self._gripperClosed = True
        self._gripperMoving = False
        self._dropped = False
            
    def sig_goBack(self, dist):
        self.signal("GoBack", dist)
         
    def sig_goForward(self, dist):
        self.signal("GoForward", dist)
         
    def sig_openGripper(self):
        self.signal("OpenGripper")
         
    def sig_lowerGripper(self):
        self.signal("LowerGripper")
        
    def sig_storeGripper(self):
        self.signal("StoreGripper")
         
    def on_paddleState(self, ps):
        self._paddleState = ps
        
    def on_gripState(self, gs):
        
        self._gripState = gs
        print('gripstate ',self._gripState)
            
    def on_gripperClosed(self):
        if (self._gripperClosed):
            self._gripperClosed = False
        elif (not self._gripperClosed) & (self._paddleState > 0):
            self._gripperClosed = True
#         print("on_gripperClosed", self._gripperClosed)
            
    def on_afterMove(self):
        self._afterMove = True
        
        
    def fire(self, currentDesired):
        print('FIRE')
        if (self._gripState == 2) & (self._afterMove == True):
            print('gripper runter und oeffnen')
            self.sig_lowerGripper()
            time.sleep(4)
            self.sig_openGripper()
            self._dropped = True
            self._gripState = 1
            
        print("gripState", self._gripState, "afterMove", self._afterMove, "dropped", self._dropped)
        if (self._gripState == 1) & (self._dropped == True):
            self.sig_storeGripper()
            self.sig_goBack(self._dist * (-1))
            self._afterMove = False
            self._dropped = False
        
        if (self._dropped == False) & (self._afterMove == True):
            self.stop()
            
    