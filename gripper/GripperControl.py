'''
Created on 20.02.2014

@author: SR
'''

from AriaPy import ArGripper
from aria_python.robot import getRobotInstance
from aria_python.adaction import AdAction

class GripperControl(AdAction):
    def __init__(self):
        AdAction.__init__(self, "GripperControl")
        self._gripper = ArGripper(getRobotInstance().arRobot)
        
    def start(self):
        print('GRIPPERCONTROL')
        AdAction.start(self)
        self._gripper_moving = False
        self._lift_moving = False
        self._bbs = self._gripper.getBreakBeamState()
        self._lift_maxed = self._gripper.isLiftMaxed()
        self._paddleState = self._gripper.getPaddleState()
        self._gripState = self._gripper.getGripState()
        print('GRIPSTATE: ', self._gripState)
        
    def fire(self, currentDesired):
        currentDesired.reset()
        ps = self._gripper.getPaddleState()
        if (ps != self._paddleState):
            self.sig_paddleState(ps)
            self._paddleState = ps

        bbs = self._gripper.getBreakBeamState()
        if (bbs != self._bbs):
            self.sig_break_beam_state_changed(bbs)
            self._bbs = bbs
            
        gripState = self._gripper.getGripState()
        print('self Gripstate: ',self._gripState, 'gripState',gripState)
        if (gripState == self._gripState):
            self.sig_gripState(gripState)
            self._gripState = gripState
            
            
        if (self._gripper.isGripMoving()):
            if not self._gripper_moving:
                self.sig_gripper_moving()
            self._gripper_moving = True
        elif self._gripper_moving:
            self._gripper_moving = False
            self.sig_gripper_stopped()
             
    def sig_gripper_moving(self):
        self.signal("gripper_moving")
    
    def sig_gripState(self, gripState):
        self.signal("GripState", gripState)
    
    def sig_gripper_stopped(self):
        self.signal("gripper_stopped")
    
    def sig_paddleState(self, ps):
        self.signal("paddleState", ps)
        
    def sig_break_beam_state_changed(self, bbs):
        self.signal("BreakBeamState", bbs)
        
    def on_openGripper(self):
        self._gripper.gripOpen()
        
    def on_lowerGripper(self):
        self._gripper.liftDown()
    
    def on_gripStore(self):
        print("gripperControl.gripStore()")
        self._gripper.gripperStore()
        
    def on_gripDeploy(self):
        self._gripper.gripperDeploy()
        
    def on_liftGripper(self):
        self._gripper.liftUp()