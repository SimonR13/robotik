'''
Created on 19.02.2014

@author: SR
'''
from AriaPy import ArGripper
from AriaPy import ArActionDesired
from aria_python.adaction import AdAction
from de.trier.function.utility import pose_dist #, pose_angle, angle_diff, clamp
import time

class ActionPickUp(AdAction):
    def __init__(self):
        AdAction.__init__(self)
        self._desired = ArActionDesired()
        self._gripper = ArGripper(self.getRobot())
        self._dist = 500
        self._speed = 100

    def start(self):
        AdAction.start(self) 
        self._startPose = self.getRobot().getPose()       
        self._beamState= self._gripper.getBreakBeamState()
        self._paddleState= self._gripper.getPaddleState()
        self._firstRun = True
        self._gripperStored = False
        self._setBack = False

    def sig_goCloser(self):
        self.signal("GoCloser")
    
    def sig_setSpeed(self, speed):
        self.signal("SetSpeed", speed)
    
    def sig_goBack(self, dist):
        self.signal("GoBack")
        
    def sig_stop(self):
        self.signal("Stop")

    def sig_storeGripper(self):
        self.signal("StoreGripper")
        
    def sig_deployGripper(self):
        self.signal("DeployGripper")
        
    def on_paddleStateChange(self, ps):
        self._paddleState = ps
        
    def on_breakBeamState(self, bbs):
        self._beamState = bbs
        if bbs == 3:
            self.sig_stop()
        
    def on_moveDoneBack(self):
        time.sleep(2)
        self._setBack = True
        
    def fire(self, currentDesired):
        currentDesired.reset()
        if (self._beamState == 0) & (self._firstRun == False):
#             print("closer; beamstate=0")
#             self.sig_setSpeed(self._speed)
#             self.sig_goCloser()
            print("desired_forward", self._speed)
            currentDesired.setVel(self._speed)
            
        if (self._beamState == 2) & (self._firstRun == False):
#             print("slow closer; beamstate==2")
            #===================================================================
            # self.sig_setSpeed(self._speed/4)
            # self.sig_goCloser()
            #===================================================================
            print("desired_forward", self._speed)
            currentDesired.setVel(self._speed / 4)

        if (self._beamState == 1 | self._beamState == 3) & (self._firstRun == False):
            print("closGripper; beamstate = 1 or 3")
            self.sig_stop()
            currentDesired.setVel(0)
            self.sig_storeGripper()
            time.sleep(3)
            self._gripperStored = True

        if (self._firstRun):
#             print("ActionGripper.FirstRun", self._gripper.isLiftMaxed(), self._gripper.isLiftMoving())
            self.sig_deployGripper()
            time.sleep(3)
            self._firstRun = False

        if (self._paddleState == 3) & (self._gripperStored == True) & (self._gripper.isGripMoving() != True) & (self._firstRun == False):
            print("back")
            currPose = self.getRobot().getPose()
            dist = pose_dist(self._startPose, currPose)
            self._dist = dist 
            if (dist - 100 <= 0) or (dist > self._dist):
                self._setBack = False
                currentDesired.setVel(0)
                self.stop()
            
            currentDesired.setVel(self._speed *(-0.5))

            print("currPose:", currPose, "startPose:", self._startPose, "dist:", dist, "currVel:", currentDesired.getVel())
                
        return currentDesired
            